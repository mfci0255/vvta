package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeMockTest {
    private Employee employee;
    EmployeeMock employeeMock = new EmployeeMock();

    @BeforeEach
    void setUp() {
        employee = new Employee();
        employee.setFirstName("Vasile");
        employee.setLastName("Ionescu");
        employee.setCnp("1234567890123");
        employee.setSalary(1000d);
        employee.setFunction(DidacticFunction.ASISTENT);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(1)
    void BBT_TC1_addEmployee() {
        assertTrue(employeeMock.addEmployee(employee),"employee data is not valid");
        System.out.println("TC1 test passed -> data valid");
    }

    @Test
    @Order(2)
    void BBT_TC2_addEmployee() {
        employee.setFunction(DidacticFunction.LECTURER);
        assertTrue(employeeMock.addEmployee(employee),"employee data is not valid");
        System.out.println("TC2 test passed -> data valid");
    }

    @Test
    @Order(3)
    void BBT_TC3_addEmployee() {
        employee.setFunction(DidacticFunction.TEACHER);
        assertTrue(employeeMock.addEmployee(employee),"employee data is not valid");
        System.out.println("TC3 test passed -> data valid");
    }

    @Test
    @Order(4)
    void BBT_TC4_addEmployee() {
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        assertTrue(employeeMock.addEmployee(employee),"employee data is not valid");
        System.out.println("TC4 test passed -> data valid");
    }

    @Test
    @Order(5)
    void BBT_TC5_addEmployee() {
        employee.setFunction(DidacticFunction.STUDENT);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC5 test passed -> data invalid");
    }

    @Test
    @Order(6)
    void BBT_TC6_addEmployee() {
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(-200d);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC6 test passed -> data invalid");
    }

    @Test
    @Order(7)
    void BBT_TC7_addEmployee() {
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(-200d);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC7 test passed -> data invalid");
    }

    @Test
    @Order(8)
    void BBT_TC8_addEmployee() {
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(-200d);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC8 test passed -> data invalid");
    }

    @Test
    @Order(9)
    void BBT_TC9_addEmployee() {
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        employee.setSalary(-200d);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC9 test passed -> data invalid");
    }

    @Test
    @Order(10)
    void BBT_TC10_addEmployee() {
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(0d);
        assertFalse(employeeMock.addEmployee(employee),"employee data is valid");
        System.out.println("TC10 test passed -> data invalid");
    }

    @Test
    @Order(11)
    void BBT_TC11_addEmployee() {
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(1d);
        assertTrue(employeeMock.addEmployee(employee),"employee data is invalid");
        System.out.println("TC11 test passed -> data valid");
    }

    @Test
    @Order(12)
    void BBT_TC12_addEmployee() {
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2d);
        assertTrue(employeeMock.addEmployee(employee),"employee data is invalid");
        System.out.println("TC12 test passed -> data valid");
    }


    @Test
    void testModifyEmployeeFunction() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee ionela = employeeMock.findEmployeeById(2);
        employeeMock.modifyEmployeeFunction(ionela,DidacticFunction.ASISTENT);
        assertEquals(DidacticFunction.ASISTENT,ionela.getFunction(),"Test failed");
        System.out.println("Test passed");
    }

    @Test
    void test2ModifyEmployeeFunction() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee test = null;
        employeeMock.modifyEmployeeFunction(test,DidacticFunction.LECTURER);
        assertNull(test,"Test failed");
        System.out.println("Test passed");
    }
}