package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    private Employee a;
    @BeforeEach
    void setUp() {
        a = new Employee();
        a.setId(1);
        a.setFirstName("Ion");
        a.setLastName("Petrescu");
        a.setCnp("1234567891111");
        a.setFunction(DidacticFunction.ASISTENT);
        a.setSalary(2000d);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(2)
    void testGetFirstName() {
        assertEquals("Ion", a.getFirstName(),"the first name is not Ion");
        System.out.println("get first name is ok");
    }

    @ParameterizedTest
    @Order(4)
    @ValueSource(strings={"Ion", "Maria"})
    void testSetFirstName(String firstName) {
        a.setFirstName(firstName);
        assertEquals(firstName,a.getFirstName(),"first name is not " + firstName);
        System.out.println("first name is " + firstName);
    }

    @Test
    @Order(1)
    void testGetLastName() {
        assertEquals("Petrescu", a.getLastName(),"the last name is not Petrescu");
        System.out.println("get last name is ok");
    }

    @ParameterizedTest
    @Order(3)
    @ValueSource(strings={"Petrescu", "Popescu"})
    void testSetLastName(String lastName) {
        a.setLastName(lastName);
        assertEquals(lastName,a.getLastName(),"last name is not " + lastName);
        System.out.println("last name is " + lastName);
    }

    @Test
    void testGetCnp() {
        assertNotEquals("123456", a.getCnp(),"the get cnp is not ok");
        System.out.println("get cnp is ok");
    }

    @ParameterizedTest
    @ValueSource(strings={"123456", "7758745"})
    void testSetCnp(String cnp) {
        a.setCnp(cnp);
        assertEquals(cnp,a.getCnp(),"cnp is not " + cnp);
        System.out.println("cnp is " + cnp);
    }
    @Test
    void testEmployee(){
       Employee b = new Employee();
        b.setFirstName("");
        b.setLastName("");
        b.setCnp("");
        b.setFunction(DidacticFunction.ASISTENT);
        b.setSalary(0.0d);
        Employee c = new Employee();
       assertEquals(b,c,"constructor is not implicit");
        System.out.println("implicit constructor is ok");
    }

    @Test
    void testSetSalary()
    {
        try{a.setSalary(4d);
            assert(true);}
        catch(Exception e)
        {
            assert(false);
        }
        System.out.println("set salary was successful");
    }

    @Test
    void test_timeout() { //if the sleep time(800) is larger than the set duration limit(1000), the test will fail
        assertTimeout(Duration.ofMillis(1000), () -> {
            Thread.sleep(800);
        });
    }
}